FROM golang:1.18 as build
COPY . /app
WORKDIR /app
RUN go mod download
RUN go build -o /golang-app cmd/main/main.go

FROM gcr.io/distroless/base-debian10 as run
WORKDIR /
COPY --from=build /golang-app /golang-app
EXPOSE 8080:8080
USER nonroot:nonroot
ENTRYPOINT ["/golang-app"]