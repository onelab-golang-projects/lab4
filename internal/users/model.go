package users

type User struct {
	Id       int    `json:"id,omitempty"`
	UserName string `json:"user_name"`
	Age      int    `json:"age"`
}
