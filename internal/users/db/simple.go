package db

import (
	"OneLab4/internal/users"
	"context"
	"fmt"
)

var simpleUsers = []users.User{
	{
		1,
		"Mykty-Ayan",
		22,
	},
	{
		2,
		"vakidzaci",
		24,
	},
	{
		3,
		"bsmiko",
		25,
	},
}

type repository struct {
	client string
}

func NewRepository(client string) users.Repository {
	return &repository{
		client: client,
	}
}

func (r repository) FindAll(ctx context.Context) ([]users.User, error) {

	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	return simpleUsers, nil
}

func (r repository) FindOne(ctx context.Context, id int) (users.User, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	userToFind := users.User{Id: -1}
	for _, user := range simpleUsers {
		if user.Id == id {
			userToFind = user
			break
		}
	}
	if userToFind.Id == -1 {
		return userToFind, fmt.Errorf("could not find user with specified id: %d", id)
	}
	return userToFind, nil
}

func (r repository) Create(ctx context.Context, u users.User) error {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	simpleUsers = append(simpleUsers, u)
	return nil
}

func (r repository) Update(ctx context.Context, u users.User) error {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	index := -1
	for i, user := range simpleUsers {
		if user.Id == u.Id {
			index = i
			break
		}
	}
	if index == -1 {
		return fmt.Errorf("could not find user with specified id: %d", u.Id)
	}
	simpleUsers[index] = u
	return nil
}

func (r repository) Delete(ctx context.Context, id int) error {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	index := -1
	for i, user := range simpleUsers {
		if user.Id == id {
			index = i
			break
		}
	}
	if index == -1 {
		return fmt.Errorf("could not find user with specified id: %d", id)
	}
	simpleUsers[index] = simpleUsers[len(simpleUsers)-1]
	simpleUsers = simpleUsers[:len(simpleUsers)-1]
	return nil
}
