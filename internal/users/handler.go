package users

import (
	"OneLab4/internal/handlers"
	"context"
	"fmt"
	"github.com/labstack/echo/v4"
	"math/rand"
	"net/http"
	"strconv"
)

const (
	usersURL = "/user/"
	userURL  = "/user/:id"
)

type handler struct {
	e          *echo.Echo
	repository Repository
}

func NewHandler(repository Repository, e *echo.Echo) handlers.Handler {
	return &handler{
		e,
		repository,
	}
}

func (h *handler) Register() {
	router := h.e.Router()
	router.Add(http.MethodGet, usersURL, h.GetList)
	router.Add(http.MethodPost, usersURL, h.Create)
	router.Add(http.MethodPut, usersURL, h.Update)
	router.Add(http.MethodGet, userURL, h.GetById)
	router.Add(http.MethodDelete, userURL, h.Delete)
}

func (h *handler) GetList(c echo.Context) error {
	h.e.Logger.Info("requested list of users")
	users, err := h.repository.FindAll(context.Background())
	if err != nil {
		return c.JSON(http.StatusBadRequest, struct {
			Message string `json:"message"`
		}{"Something went wrong"})
	}
	return c.JSON(http.StatusOK, users)
}

func (h *handler) GetById(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, struct {
			Message string `json:"message"`
		}{fmt.Sprintf("Could not convert id: %s", c.Param("id"))})
	}
	user, err := h.repository.FindOne(context.TODO(), id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, struct {
			Message string `json:"message"`
		}{fmt.Sprintf("Could found specified id: %d", id)})
	}
	return c.JSON(http.StatusOK, user)
}

func (h *handler) Create(c echo.Context) error {
	u := &User{Id: rand.Int()}

	if err := c.Bind(u); err != nil {
		return c.JSON(http.StatusBadRequest, struct {
			Message string `json:"message"`
		}{"Something went wrong with unmarshalling"})
	}

	err := h.repository.Create(context.TODO(), *u)
	if err != nil {
		return c.JSON(http.StatusBadRequest, struct {
			Message string `json:"message"`
		}{"Could not insert record"})
	}

	return c.JSON(http.StatusCreated, u)
}

func (h *handler) Update(c echo.Context) error {
	u := &User{Id: rand.Int()}

	if err := c.Bind(u); err != nil {
		return c.JSON(http.StatusBadRequest, struct {
			Message string `json:"message"`
		}{"Something went wrong with unmarshalling"})
	}

	err := h.repository.Update(context.TODO(), *u)
	if err != nil {
		return c.JSON(http.StatusBadRequest, struct {
			Message string `json:"message"`
		}{"Could not update record"})
	}

	return c.JSON(http.StatusNoContent, nil)
}

func (h *handler) Delete(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, struct {
			Message string `json:"message"`
		}{fmt.Sprintf("Could not convert id: %s", c.Param("id"))})
	}

	err = h.repository.Delete(context.TODO(), id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, struct {
			Message string `json:"message"`
		}{"Could not delete record"})
	}

	return c.JSON(http.StatusNoContent, nil)
}

func (h *handler) SortByYear(c echo.Context) error {
	// TODO implement sorting by year
	panic("implement me!")
}
