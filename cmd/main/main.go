package main

import (
	"OneLab4/internal/users"
	"OneLab4/internal/users/db"
	"github.com/labstack/echo/v4"
	"net/http"
)

func main() {
	e := echo.New()

	e.GET("/", func(c echo.Context) error {
		e.Logger.Info("")
		return c.HTML(http.StatusOK, `
			<!DOCTYPE html>
			<html>
			<head></head>
			<body>
				<h1> Simple Golang Echo Project </h1>
				<p> This project contains information about User and Book.
			</body>
			</html>`)
	})

	userRepository := db.NewRepository("Emulating postgres client")
	userHandler := users.NewHandler(userRepository, e)
	userHandler.Register()

	e.Logger.Fatal(e.Start("0.0.0.0:8080"))
}
